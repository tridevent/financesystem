
public class Windows {
	public static MainScreen mainScreen = new MainScreen();
	public static Consult consultScreen = new Consult();
	public static Operation operationScreen = new Operation();
	
	public static void MainShow()
	{
		mainScreen.setVisible(true);
	}
	public static void ConsultShow()
	{
		consultScreen.setVisible(true);
	}
	public static void OperationShow() {
		operationScreen.setVisible(true);
	}
}
