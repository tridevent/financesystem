//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import DB.Methods;


public class Operation extends JFrame {

	private JPanel contentPane;
	private JTextField nameTxt;
	private JTextField amountTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Operation frame = new Operation();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Operation() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblName = new JLabel("Nombre");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JLabel lblAmount = new JLabel("Monto");
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		nameTxt = new JTextField();
		nameTxt.setHorizontalAlignment(SwingConstants.CENTER);
		nameTxt.setColumns(10);
		
		amountTxt = new JTextField();
		amountTxt.setHorizontalAlignment(SwingConstants.CENTER);
		amountTxt.setColumns(10);
		
		JButton btnBack = new JButton("Volver");
		// eventos
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Windows.MainShow();
				dispose();
			}
		});
		
		final JComboBox cbEntry = new JComboBox();
		cbEntry.setFont(new Font("Tahoma", Font.PLAIN, 15));
		cbEntry.setModel(new DefaultComboBoxModel(new String[] {"Ingreso", "Egreso"}));
		
		final JComboBox cbType = new JComboBox();
		cbType.setModel(new DefaultComboBoxModel(new String[] {"Fijo", "Unico"}));
		cbType.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton btnAccept = new JButton("Aceptar");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean entry;
				if(cbEntry.getSelectedIndex()==0)
					entry=true;
				else
					entry=false;
				//Data.AddData(nameTxt.getText(),Float.parseFloat(amountTxt.getText()),entry, cbType.getSelectedIndex());
				Methods mtd= new Methods();
				int id = mtd.Count("SELECT id FROM balancedetail ORDER BY id DESC");
				id++;
				System.out.println(id);
				mtd.Insert("insert into balancedetail values("+id+",'"+nameTxt.getText() +"',"+
				Float.parseFloat(amountTxt.getText())+","+ entry+","+cbType.getSelectedIndex()+")");
			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblName)
							.addGap(18)
							.addComponent(nameTxt, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnBack)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblAmount, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(cbEntry, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(cbType, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
								.addComponent(amountTxt, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
								.addComponent(btnAccept, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
					.addContainerGap(103, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblName)
						.addComponent(nameTxt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAmount, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(amountTxt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(cbEntry, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(cbType, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addGap(39)
					.addComponent(btnAccept)
					.addPreferredGap(ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
					.addComponent(btnBack)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
