//ejemplos
/*System.exit(0); para salir del programa 
 	JOptionPane.showMessageDialog(null,"texto"); para abrir un pop up
 	ButtonGroup botones = new ButtonGroup();
 	botones.add(miboton); sirve para agrupar botones y que no se permita la multiseleccion.
  	import java.util.Vector;
    Vector<String> v = new Vector<String>();
    v.add("Hola");
    v.add("Este es el segundo elemento del vector");
    v.add("Tercera l�nea");
    System.out.println("elementos en v : "+v.size());
    System.out.println("primer elemento: "+v.elementAt(0));*/
import java.util.Vector;

import javax.swing.JOptionPane;
public class Data {
	public static Float balance;
	public static Vector<String> names = new Vector<String>();
	public static Vector<Float> amounts = new Vector<Float>();
	public static Vector<Boolean> entrys = new Vector<Boolean>();
	public static Vector<Integer> typeEntrys = new Vector<Integer>();
	
	public static void AddData(String name, Float amount, Boolean entry, Integer type)
	{
		names.add(name);
		amounts.add(amount);
		entrys.add(entry);
		typeEntrys.add(type);
		if(entry)
			balance+= amount;
		else
			balance-=amount;
		System.out.println("Agregado: "+ name + " "+ amount + " ingreso: "+ entry + " tipo: "+ type);
		JOptionPane.showMessageDialog(null, "Agregado: "+ name + " "+ amount + " ingreso: "+ entry + " tipo: "+ type);
	}
}
